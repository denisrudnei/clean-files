import Router from 'vue-router'
import Vue from 'vue'
import Home from './components/home'
import Config from './components/config'

Vue.use(Router)

const routes = new Router({
  routes: [
    {
      path: '/config',
      component: Config
    },
    {
      path: '/',
      component: Home
    }
  ]
})

export default routes
